/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Dec 2, 2016 8:31:49 PM                      ---
 * ----------------------------------------------------------------
 */
package com.shaneco.initialdata.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedShanecoInitialDataConstants
{
	public static final String EXTENSIONNAME = "shanecoinitialdata";
	
	protected GeneratedShanecoInitialDataConstants()
	{
		// private constructor
	}
	
	
}
